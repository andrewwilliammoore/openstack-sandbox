# Configure the OpenStack Provider
provider "openstack" {}

variable "PROVIDER_PUBLIC_SSH_KEY" {}

# Configure the public key pair
resource "openstack_compute_keypair_v2" "ssh_key" {
  name       = "fuga-instance-key"
  public_key = var.PROVIDER_PUBLIC_SSH_KEY
}
