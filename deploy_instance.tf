# Creating an Instance
resource "openstack_compute_instance_v2" "server-1" {
  name            = "server-1"
  flavor_name     = "t2.micro"
  key_pair        = openstack_compute_keypair_v2.ssh_key.name
  security_groups = ["default"]

  block_device {
    uuid                  = openstack_blockstorage_volume_v3.boot_server-1.id
    source_type           = "volume"
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
  }

  network {
    name = "public"
  }
}

# Create a bootable-volume with CentOS
resource "openstack_blockstorage_volume_v3" "boot_server-1" {
  region      = "ams"
  name        = "boot_server-1"
  description = "system volume for server-1"
  size        = 20

  # Ubuntu 18.04 LTS
  image_id = "b88f311a-b5f6-49d0-ad44-5304f9a8ef9b"
}
