terraform {
  required_version = "~> 0.12.8"

  backend "swift" {
    container = "openstack-sandbox-backend"
  }
}
