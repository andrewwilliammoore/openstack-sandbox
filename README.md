# openstack-sandbox

Trying out OpenStack with Fuga Cloud and Terraform. Tutorial at https://docs.fuga.cloud/how-to-deploy-an-instance-using-terraform
Versions:
- Terraform: 0.12.8
- provider.openstack v1.28.0

## Setup
Download an OpenRc file from provider for credentials. Place it wherever and note the path. Your provider will give a password/key that will be needed when establishing credentials. Keep this someplace safe!
```bash
source PATH_TO_OPEN_RC_FILE
```

Create an ssh key named fuga-instance for a new compute instance.

Set the following env vars (there is a .gitignore entry for an .envrc):
```bash
export TF_VAR_PROVIDER_PUBLIC_SSH_KEY="*PUBLIC_KEY_VALUE*";
```

## Deploy/Teardown
Create the infrastructure:
```bash
terraform apply
```

Teardown the infrastructure:
```bash
terraform destroy
```
